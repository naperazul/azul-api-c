using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Diagnostics.Process;
using System.Web.AspNetHostingPermission;
using System.Media;
namespace API
{
	public class Azul_api
	{
		//#######################################################################
		//#######################################################################
		//######################## Azul API for developers #################################
		//#Cet classe vous facilite la programmation d'une application qui sera optimis� avec l'interface graphique de Azul .
		//En utilisant le c# ou le vb , vous serai faciliment capable de tourn� et lanc� votre propore application .
		//Azul Team , inc .
		//Api for vb.net
		//#######################################################################
		//#######################################################################
		//######################## Azul API for developers #################################

		#region "Your_APP_INFO"
		//Informations sur votre application

			//Nom de votre application
		public const  APP_name = "My_APP";
			//Icone de votre application
		public const  APP_icone = "";
			//Version de votre application
		public const  APP_version = "1.0.0";
			// Maj de votre application
		public const  APP_maj_link = "www.azul.com";
			// Description de votre application
		public const  APP_description = "";
			// Le type de votre application
		public const  APP_type = "Application";

		//Informations Azul_your_app

			// Si votre application s'affichera dans la bare de Azul
		public const  Azul_app_menu = false;
			//Si votre application s'affichera dans le menu start de Azul
		public const  Azul_app_start = false;
			//D�termine le niveau d'autorisation d'acc�s au fichier
		public const  Azul_app_infos_right = "AZ_cl";
			//Determine si les erreurs de votre applictaion f'afficheron avec la function azul_errors
		public const  Azul_app_error = true;
			//Si votre application utlisera l'internet .
		public const  Azul_app_useconnexion = false;
			//Si votre applciation se connectera aux serveurs d'Azul .
		public const  Azul_app_useazserver = false;
			//Si votre app cr�era une nouvelle bare .
		public const  Azul_app_newbare = false;
			//Votre application peux utiliser la function azul_capture pour capturer le screen
		public const  Azul_app_capturscreen = false;

		//Declarer

		// Public Shared APP_ip = az_connect.LocalIP
		public static string az_data = "";
		public static Socket SocketClient;
		public static bool LocalsocketClientIsShutingDown;
		public static byte[] readbuf;
		public static byte[] sendbuf;

		public static ListBox listBox;
		public delegate void DelegateDisplay(string message);
		//  Public Shared dlgDisplay As DelegateDisplay = New DelegateDisplay(AddressOf DisplayMessage)
		#endregion

		#region "Azul sockets"

		public Azul_api(System.Windows.Forms.ListBox list)
		{
			readbuf = new byte[101];
		}

		public static void ConnectToServer(string ServerName)
		{
			try {
				if (SocketClient == null || !SocketClient.Connected) {
					SocketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					IPAddress[] ipadress = null;
					IPHostEntry he = Dns.GetHostByName(ServerName);
					ipadress = ipadress;
					SocketClient.BeginConnect(new IPEndPoint(ipadress[0], 15), ConnectCallback, SocketClient);

				} else {
					string[] obj = new string[] { "D�j� connect� � Azul" };

				}
			} catch (Exception ex) {
				Interaction.MsgBox("Azul n'est pas ouvert pour lanc� votre application ." + Constants.vbCrLf + ex.ToString());
				Application.Exit();
			}

		}

		private static void ConnectCallback(IAsyncResult asyncResult)
		{
			string[] obj = null;
			try {
				Socket socket = (Socket)asyncResult.AsyncState;
				SocketClient = socket;
				socket.EndConnect(asyncResult);
				obj = new string[] { "Connect�  � Azul" };
				//  listBox.Invoke(dlgDisplay, obj)
				LocalsocketClientIsShutingDown = false;
				SocketClient.BeginReceive(readbuf, 0, readbuf.Length, SocketFlags.None, ReceiveCallback, SocketClient);
				send_data();
			} catch (SocketException ex) {
				obj = new string[] { ex.Message };

			}
		}

		public static void SendMessage(string message)
		{
			if ((SocketClient != null) && SocketClient.Connected) {
				sendbuf = Encoding.ASCII.GetBytes(message);
				SocketClient.BeginSend(sendbuf, 0, sendbuf.Length, SocketFlags.None, SendCallback, SocketClient);
			} else {
				Interaction.MsgBox("Non connect� � Azul.");
			}
		}

		private static void SendCallback(IAsyncResult asyncResult)
		{
			string[] obj = null;
			try {
				Socket socket = (Socket)asyncResult.AsyncState;
				int send = socket.EndSend(asyncResult);
				obj = new string[] { "Message envoy� (" + send.ToString() + " bytes envoy�s )" };
			//    listBox.Invoke(dlgDisplay, obj)
			} catch (SocketException ex) {
				obj = new string[] { ex.Message };

			}
		}

		public static void ReceiveMessage()
		{
			if ((SocketClient != null) && SocketClient.Connected) {
				SocketClient.BeginReceive(readbuf, 0, readbuf.Length, SocketFlags.None, ReceiveCallback, SocketClient);
			} else {
				Interaction.MsgBox("Non connect� � azul.");
			}
		}

		private static void ReceiveCallback(IAsyncResult asyncResult)
		{
			string[] obj = null;
			try {
				Socket socket = (Socket)asyncResult.AsyncState;
				int read = socket.EndReceive(asyncResult);
				if (read > 0) {
					obj = new string[] { "Azul dit :" + Encoding.ASCII.GetString(readbuf) };
					//  listBox.Invoke(dlgDisplay, obj)
					SocketClient.BeginReceive(readbuf, 0, readbuf.Length, SocketFlags.None, ReceiveCallback, SocketClient);
				}
				if (read == 0 && !LocalsocketClientIsShutingDown) {
					SocketClient.Close();
					obj = new string[] { "Fermeture socket distante" };

				}
				Buffer.SetByte(readbuf, 0, 0);
			} catch (SocketException ex) {
				obj = new string[] { ex.Message };

			}
		}

		public static void Close()
		{
			if ((SocketClient != null) && SocketClient.Connected) {
				LocalsocketClientIsShutingDown = true;
				SocketClient.Shutdown(SocketShutdown.Both);
				SocketClient.Close();
				string[] obj = new string[] { "Connexion ferm�e" };
			}
		}
		#endregion

		#region "Azul_function"
		public static void start()
		{
			ConnectToServer(true);
		}

		public static void send_data()
		{
			try {
				SendMessage("azul_app_identification");
				send_app_infos();
			} catch (Exception ex) {
				Interaction.MsgBox(ex.ToString());
			}

		}
		public static void send_app_infos()
		{
			try {
				SendMessage("APP_infos");
				SendMessage("name=" + APP_name);
				SendMessage("icone=" + APP_icone);
				SendMessage("version=" + APP_version);
				SendMessage("Type =" + APP_type);
				SendMessage("description=" + APP_description);
				send_azul_data();
			} catch (Exception ex) {
				Interaction.MsgBox(ex.ToString());
			}

		}
		public static void send_azul_data()
		{
			try {
				SendMessage("Azul_infos");
				traiterdata();
			// az_connect.SendData("Azul_app_menu=" & Azul_app_menu)
			// az_connect.SendData("Azul_app_capturscreen=" & Azul_app_capturscreen)
			//  az_connect.SendData("Azul_app_infos_right=" & Azul_app_infos_right)
			//  az_connect.SendData("Azul_app_error=" & Azul_app_error)
			// az_connect.SendData("Azul_app_newbare=" & Azul_app_newbare)
			// az_connect.SendData("Azul_app_start=" & Azul_app_start)
			// az_connect.SendData("Azul_app_useazserver=" & Azul_app_useazserver)
			// az_connect.SendData("Azul_app_useconnexion=" & Azul_app_useconnexion)

			} catch (Exception ex) {
				Interaction.MsgBox(ex.ToString());
			}
		}

		public static void doo()
		{
		}

		#endregion

		#region "Azul_data"
		public static void traiterdata()
		{
			if (Azul_app_capturscreen == true) {
				SendMessage("cp_ok");
			} else {
				SendMessage("cp_no");
			}
			if (Azul_app_error == true) {
				SendMessage("er_ok");
			} else {
				SendMessage("er_no");
			}
			if (Azul_app_infos_right == "AZ_cl") {
				SendMessage("ir_cl");
			} else {
				SendMessage("ir_ad");
			}
			if (Azul_app_menu == true) {
				SendMessage("m_ok");
			} else {
				SendMessage("m_no");

			}
			if (Azul_app_newbare == true) {
				SendMessage("nb_ok");
			} else {
				SendMessage("nb_no");
			}
			if (Azul_app_start == true) {
				SendMessage("st_ok");
			} else {
				SendMessage("st_no");
			}
			if (Azul_app_useazserver == true) {
				SendMessage("us_ok");

			} else {
				SendMessage("us_no");
			}
			if (Azul_app_useconnexion == true) {
				SendMessage("uc_ok");
			} else {
				SendMessage("uc_no");
			}
		}
		#endregion

		#region "Azul_files"

		public static void informations()
		{
		}
		#endregion

	}
}